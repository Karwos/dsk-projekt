package com.dsk.todo.repositories;

import com.dsk.todo.models.ToDo;
import org.springframework.data.repository.CrudRepository;


public interface ToDoRepository extends CrudRepository<ToDo, Long> {
}
