package com.dsk.todo.controllers;

import com.dsk.todo.models.ToDo;
import com.dsk.todo.models.ToDoDTO;
import com.dsk.todo.services.ToDoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ToDoController {

    private ToDoService toDoService;

    @Autowired
    public ToDoController(ToDoService toDoService) {
        this.toDoService = toDoService;
    }

    @GetMapping("/to-do")
    public ResponseEntity<List<ToDo>> getToDos() {
        return new ResponseEntity<>(toDoService.getToDos(), HttpStatus.OK);
    }

    @PostMapping("/to-do")
    public ResponseEntity<ToDo> createToDo(@Valid @RequestBody ToDoDTO toDo) {
        return new ResponseEntity<>(toDoService.createToDo(toDo), HttpStatus.OK);
    }

    @GetMapping("/to-do/{id}")
    public ResponseEntity<ToDo> getToDo(@PathVariable Long id) {
        ToDo todo = toDoService.getToDo(id);
        if (todo == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        return new ResponseEntity<>(toDoService.getToDo(id), HttpStatus.OK);
    }

    @PutMapping("/to-do/{id}")
    public ResponseEntity<ToDo> updateToDo(@PathVariable Long id,
                                           @Valid @RequestBody ToDoDTO toDoDTO) {
        return new ResponseEntity<>(toDoService.updateToDo(id, toDoDTO), HttpStatus.OK);
    }
}
