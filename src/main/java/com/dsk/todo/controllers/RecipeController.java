package com.dsk.todo.controllers;

import com.dsk.todo.models.Ingredient;
import com.dsk.todo.models.Recipe;
import com.dsk.todo.models.RecipeDTO;
import com.dsk.todo.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RecipeController {

    private RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @GetMapping("/recipes")
    public ResponseEntity<?> getRecipes() {
        return new ResponseEntity<>(recipeService.getRecipes(), HttpStatus.OK);
    }

    @PostMapping("/recipes")
    public ResponseEntity<?> createRecipe(@Valid @RequestBody RecipeDTO recipeDTO) {
        RecipeDTO responseRecipe = recipeService.createRecipe(recipeDTO);
        return new ResponseEntity<>(responseRecipe, HttpStatus.CREATED);
    }

    @GetMapping("/recipes/{id}")
    public ResponseEntity<RecipeDTO> getRecipes(@PathVariable Long id) {
        return new ResponseEntity<>(recipeService.getRecipe(id), HttpStatus.OK);
    }

    @PutMapping("/recipes/{id}")
    public ResponseEntity<?> updateRecipe(@PathVariable Long id,
                                          @RequestBody Recipe recipe) {
        return new ResponseEntity<>(recipeService.updateRecipe(id, recipe), HttpStatus.OK);
    }

    @DeleteMapping("/recipes/{id}")
    public ResponseEntity<?> deleteRecipe(@PathVariable Long id) {
        recipeService.deleteRecipe(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PutMapping("/recipes/{id}/addIngredient")
    public ResponseEntity<RecipeDTO> addIngredientToRecipe(@PathVariable Long id,
                                                           @RequestBody Ingredient ingredient) {
        return new ResponseEntity<>(recipeService.addIngredient(id, ingredient), HttpStatus.OK);
    }
}
