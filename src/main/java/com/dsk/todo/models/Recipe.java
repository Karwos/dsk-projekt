package com.dsk.todo.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "Recipe")
public class Recipe {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "recipe")
    private List<Ingredient> ingredients;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_created")
    private Date created;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_modified")
    private Date lastModify;
}
