package com.dsk.todo.models;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class ToDo {

    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    private Date planedTimeToComplete;
    @Column(name = "is_done")
    private Boolean isDone;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_created")
    private Date created;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="date_modified")
    private Date lastModify;
}
