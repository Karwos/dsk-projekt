package com.dsk.todo.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Ingredient")
public class Ingredient {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "fk_recipe")
    private Recipe recipe;
    private String name;
    private Double amount;
    private String units;

}
