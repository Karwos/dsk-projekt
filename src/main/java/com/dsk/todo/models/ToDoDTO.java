package com.dsk.todo.models;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class ToDoDTO {

    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private Date planedTimeToComplete;
    @NotNull
    private Boolean isDone;
}
