package com.dsk.todo.models;

import lombok.Data;

import java.util.List;

@Data
public class RecipeDTO {

    private Long id;
    private String name;
    private String description;
    private List<IngredientDTO> ingredients;
}
