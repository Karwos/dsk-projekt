package com.dsk.todo.services;

import com.dsk.todo.models.Ingredient;
import com.dsk.todo.models.Recipe;
import com.dsk.todo.models.RecipeDTO;

import java.util.List;

public interface RecipeService {

    RecipeDTO createRecipe(RecipeDTO recipe);
    RecipeDTO getRecipe(Long id);
    List<RecipeDTO> getRecipes();
    Recipe updateRecipe(Long id, Recipe recipe);
    void deleteRecipe(Long id);
    RecipeDTO addIngredient(Long id, Ingredient ingredient);
    Recipe deleteIngredient(Long recipeId, Long ingredientId);
}
