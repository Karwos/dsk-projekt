package com.dsk.todo.services;

import com.dsk.todo.models.Ingredient;
import com.dsk.todo.models.IngredientDTO;
import com.dsk.todo.models.Recipe;
import com.dsk.todo.models.RecipeDTO;
import com.dsk.todo.repositories.IngredientRepository;
import com.dsk.todo.repositories.RecipeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {

    private RecipeRepository recipeRepository;
    private IngredientRepository ingredientRepository;
    private ModelMapper modelMapper;
    private Type listType = new TypeToken<List<IngredientDTO>>() {}.getType();

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository,
                             IngredientRepository ingredientRepository,
                             ModelMapper modelMapper) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public RecipeDTO createRecipe(RecipeDTO recipeDTO) {
        Recipe recipe = modelMapper.map(recipeDTO, Recipe.class);
        Recipe recipeDB = recipeRepository.save(recipe);
        RecipeDTO dto = modelMapper.map(recipeDB, RecipeDTO.class);
        if(recipeDB.getIngredients() != null) {
            List<IngredientDTO> ingredientDTOS = modelMapper.map(recipeDB.getIngredients(), listType);
            dto.setIngredients(ingredientDTOS);
        }
        return dto;
    }

    @Override
    public RecipeDTO getRecipe(Long id) {
        Recipe recipe = recipeRepository.findById(id).orElse(null);
        if (recipe != null) {
            RecipeDTO dto = modelMapper.map(recipe, RecipeDTO.class);
            List<IngredientDTO> ingredientDTOS = new ArrayList<>();
            for (Ingredient i: recipe.getIngredients())
                ingredientDTOS.add(modelMapper.map(i, IngredientDTO.class));
            dto.setIngredients(ingredientDTOS);
            return dto;
        }
        return null;
    }

    @Override
    public List<RecipeDTO> getRecipes() {
        List<Recipe> recipesList = recipeRepository.findAll();
        List<RecipeDTO> recipeDTOS = new ArrayList<>();
        for (Recipe r: recipesList) {
            RecipeDTO dto = modelMapper.map(r, RecipeDTO.class);
            List<IngredientDTO> ingredientDTOS = modelMapper.map(r.getIngredients(), listType);
            dto.setIngredients(ingredientDTOS);
            recipeDTOS.add(dto);
        }
        return recipeDTOS;
    }

    @Override
    public Recipe updateRecipe(Long id, Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    @Override
    public void deleteRecipe(Long id) {
        recipeRepository.deleteById(id);
    }

    @Override
    public RecipeDTO addIngredient(Long id, Ingredient ingredient) {
        Recipe recipe = recipeRepository.findById(id).orElse(null);
        if (recipe != null) {
            ingredient.setRecipe(recipe);
            Ingredient ingredient1 = ingredientRepository.save(ingredient);
            recipe.getIngredients().add(ingredient1);
            Recipe recipeDB = recipeRepository.save(recipe);

            List<IngredientDTO> ingredientDTOS = modelMapper.map(recipeDB.getIngredients(), listType);
            RecipeDTO recipeDTO = modelMapper.map(recipeDB, RecipeDTO.class);
            recipeDTO.setIngredients(ingredientDTOS);
            return recipeDTO;
        }
        return null;
    }

    @Override
    public Recipe deleteIngredient(Long recipeId, Long ingredientId) {
        Recipe recipe = recipeRepository.findById(recipeId).orElse(null);
        if (recipe != null) {
            Ingredient ingredient = recipe.getIngredients().stream().filter(i -> i.getId().equals(ingredientId)).findFirst().orElse(null);
            if (ingredient != null) {
                recipe.getIngredients().remove(ingredient);
                ingredientRepository.deleteById(ingredientId);
                return recipeRepository.save(recipe);
            }
        }
        return null;
    }
}
