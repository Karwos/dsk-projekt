package com.dsk.todo.services;

import com.dsk.todo.models.ToDo;
import com.dsk.todo.models.ToDoDTO;

import java.util.List;

public interface ToDoService {

    List<ToDo> getToDos();
    ToDo createToDo(ToDoDTO toDo);
    ToDo getToDo(Long id);
    ToDo updateToDo(Long id, ToDoDTO toDo);
}
