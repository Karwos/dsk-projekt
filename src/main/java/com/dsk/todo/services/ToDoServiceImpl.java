package com.dsk.todo.services;

import com.dsk.todo.models.ToDo;
import com.dsk.todo.models.ToDoDTO;
import com.dsk.todo.repositories.ToDoRepository;
import com.dsk.todo.services.ToDoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToDoServiceImpl implements ToDoService {

    private ToDoRepository toDoRepository;
    private ModelMapper mapper;

    @Autowired
    public ToDoServiceImpl(ToDoRepository toDoRepository,
                           ModelMapper mapper) {
        this.toDoRepository = toDoRepository;
        this.mapper = mapper;
    }

    @Override
    public List<ToDo> getToDos() {
        return (List<ToDo>) toDoRepository.findAll();
    }

    @Override
    public ToDo createToDo(ToDoDTO toDoDTO) {
        ToDo toDo = mapper.map(toDoDTO, ToDo.class);
        return toDoRepository.save(toDo);
    }

    @Override
    public ToDo getToDo(Long id) {
        return toDoRepository.findById(id).orElse(null);
    }

    @Override
    public ToDo updateToDo(Long id, ToDoDTO toDoDTO) {
        ToDo toDoDB = toDoRepository.findById(id).orElse(null);
        if (toDoDTO != null){
            if(!mapper.map(toDoDB, ToDoDTO.class).equals(toDoDTO)){
                ToDo newToDo = mapper.map(toDoDTO, ToDo.class);
                newToDo.setId(toDoDB.getId());
                newToDo.setCreated(toDoDB.getCreated());
                newToDo.setLastModify(toDoDB.getLastModify());
                return toDoRepository.save(newToDo);
            }
        }
        return null;
    }
}
